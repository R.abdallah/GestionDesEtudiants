package com.ssiar;

import com.ssiar.dao.EtudiantRepository;
import com.ssiar.entities.Etudiant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@SpringBootApplication
public class GestionDesEtudiantsApplication {

	public static void main(String[] args) throws ParseException {


        ApplicationContext ctx=SpringApplication.run(GestionDesEtudiantsApplication.class, args);
        EtudiantRepository etudiantRepository =ctx.getBean(EtudiantRepository.class);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        //etudiantRepository.save(new Etudiant("Raiss" ,"Abdallah","a.raiss@gmail.com",df.parse("1996-12-02"),"0670203040","abdallah.jpg"));
        //etudiantRepository.save(new Etudiant("Benkachoud" ,"Hicham","h.benkachoud@gmail.com",df.parse("1996-11-03"),"0670203047","hicham.jpg"));
        //etudiantRepository.save(new Etudiant("Hamrrass" ,"Hassan","h.hamrrass@gmail.com",df.parse("1993-06-07"),"0671203740","hassan.jpg"));
        //etudiantRepository.save(new Etudiant("Souffi" ,"Jamal","j.soffi@gmail.com",df.parse("1991-07-27"),"067022461","jamal.jpg"));

       // Page<Etudiant> etudiants =etudiantRepository.chercherEtudiants("%B%",new PageRequest(0,5));
       // etudiants.forEach(etudiant -> System.out.println(etudiant.getNom()));
	}
}
