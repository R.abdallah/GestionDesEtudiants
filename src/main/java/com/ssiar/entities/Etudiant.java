package com.ssiar.entities;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by raiss abdallah on 22/04/2018.
 */
@Entity
public class Etudiant implements Serializable{
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "NOM",length = 30)
    @NotEmpty
    @Size(min = 5,max = 30)
    private String nom;
    @Column(name = "PRENOM",length = 15)
    @NotEmpty
    @Size(min = 4 ,max = 15)
    private String prenom;
    @Column(name = "EMAIL",length = 30)
    @Email
    @NotEmpty
    private String email;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNaissance;
    @Column(name = "TELEPHONE",length = 10)
    @Pattern(regexp="(^$|[0-9]{10})")
    @NotEmpty
    private String telephone;
    private String photo;

    public Etudiant() {
    }

    public Etudiant(String nom, String prenom, String email, Date dateNaissance, String telephone, String photo) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.dateNaissance = dateNaissance;
        this.telephone = telephone;
        this.photo = photo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
