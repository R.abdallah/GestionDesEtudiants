package com.ssiar.web;

import com.ssiar.dao.EtudiantRepository;
import com.ssiar.entities.Etudiant;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by Raiss Abdallah on 22/04/2018.
 */
@Controller
@RequestMapping(value = "/Etudiant")
public class EtudiantController {
    @Value("${dir.images}")
    private  String pathImages;
    @Value("${var}")
    public  String variable;
    @Autowired
    private EtudiantRepository etudiantRepository;
    @RequestMapping(value = "/Index")
     public String Index(Model model,
                         @RequestParam(name = "page",defaultValue = "0") int p,
                         @RequestParam(name = "motCle",defaultValue = "")String mc ){
        Page<Etudiant> pageEtudiants =etudiantRepository.chercherEtudiants("%"+mc+"%",new PageRequest(p,5));
        model.addAttribute("pageEtudiants",pageEtudiants);
        int pageCount=pageEtudiants.getTotalPages();
        int [] pages = new int [pageCount];
        for (int i =0;i<pageCount;i++)pages[i]=i;
        model.addAttribute("pages",pages);
        model.addAttribute("varab",variable);
        model.addAttribute("pageCourante",p);
        model.addAttribute("NombrePage",pageCount);
        model.addAttribute("motCle",mc);
       return "etudiants";
     }

     @RequestMapping(value = "/form",method = RequestMethod.GET)
     public String formEtudiant(Model model){
         model.addAttribute("etudiant",new Etudiant());
         return "FormEtudiant";
     }

    @RequestMapping(value = "/SaveEtudiant",method = RequestMethod.POST)
    public String save(@Valid Etudiant etudiant,
                       BindingResult bindingResult,
                       @RequestParam(name = "picture") MultipartFile file) throws IOException {
        if(bindingResult.hasErrors()){
            return "FormEtudiant";
        }
        if(!(file.isEmpty())){
            etudiant.setPhoto(file.getOriginalFilename());
        }
        etudiantRepository.save(etudiant);
        if (!(file.isEmpty())){
            //etudiant.setPhoto(file.getOriginalFilename());
            // vous devriez avoir un dossier quelque part dans C://user/abdallah/ici qui s'appel gestionEtudiantSrc
            file.transferTo(new File(pathImages+"/"+etudiant.getId()));
        }

        return "redirect:Index";
    }
    // Methode qui permet de retrnée la photo
    @RequestMapping(value = "/getPhoto",produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public byte[] getPhoto (Long id) throws Exception {
        File file = new File(pathImages+id);
        return IOUtils.toByteArray(new FileInputStream(file));
    }
    @RequestMapping(value = "/supprimer")
    public String Supprimer(Long id){
        etudiantRepository.deleteById(id);
        return "redirect:Index";
    }
    @RequestMapping(value = "/edit")
    public String edit(Long id,Model model){
        Etudiant etudiant=etudiantRepository.getOne(id);
        model.addAttribute("etudiant",etudiant);
        return "EditEtudiant";
    }
    @RequestMapping(value = "/UpdateEtudiant",method = RequestMethod.POST)
    public String update(@Valid Etudiant etudiant,
                       BindingResult bindingResult,
                       @RequestParam(name = "picture") MultipartFile file) throws IOException {
        if(bindingResult.hasErrors()){
            return "EditEtudiant";
        }
        if(!(file.isEmpty())){
            etudiant.setPhoto(file.getOriginalFilename());
        }
        etudiantRepository.save(etudiant);
        if (!(file.isEmpty())){
            //etudiant.setPhoto(file.getOriginalFilename());
            // vous devriez avoir un dossier quelque part dans C://user/abdallah/ici qui s'appel gestionEtudiantSrc
            file.transferTo(new File(pathImages+"/"+etudiant.getId()));
        }

        return "redirect:Index";
    }
}
