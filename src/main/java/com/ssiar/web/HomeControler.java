package com.ssiar.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by abdallah raiss on 28/04/2018.
 */
@Controller
public class HomeControler {
    @GetMapping("/index")
    public String Index(){
        return "index";
    }
}
